from __future__ import division
from __future__ import print_function
import sys

import numpy as np
from matplotlib import pyplot as plt

#########################################################
#Bresenham's line algorithm                             #
#an easy and interactive implementation                 #
#                                                       #
#first enter the dimensions of the square image         #
#then enter the coordinates of the start and endpoint   #
#                                                       #
#Kai Benning 2018                                       #
#########################################################

print("")
print("Bresenham's line algorithm")
print("read-in:")
print("")
image_dimension = int(raw_input("image dimension (square): "))
print("")
if image_dimension <= 0:
    sys.exit("Error: image dimension must be a positive integer")
x1 = int(raw_input("x1: "))
if x1 < 0 or x1 > image_dimension-1:
    sys.exit("Error: outside bounds")
y1 = int(raw_input("y1: "))
if y1 < 0 or y1 > image_dimension-1:
    sys.exit("Error: outside bounds")
x2 = int(raw_input("x2: "))
if x2 < 0 or x2 > image_dimension-1:
    sys.exit("Error: outside bounds")
y2 = int(raw_input("y2: "))
if y2 < 0 or x2 > image_dimension-1:
    sys.exit("Error: outside bounds")

image = np.zeros((image_dimension,image_dimension))

print("")
print("draw line from: ")
print("start_point:","(",x1,",",y1,")")
print("end_point:","(",x2,",",y2,")")
print("")

dx = x2-x1
dy = y2-y1
D = 2*dy - dx
D_init = D
y = y1

if x1 > x2:
    x1,x2 = x2,x1
    y1,y2 = y2,y1

for x in range(x1,x2+1):
    image[x][y] = 1
    if D>0:
        y = y + 1
        D = D - 2*dx
    D = D + 2*dy

plt.figure()
plt.imshow(image, cmap='gray')
plt.xlabel("x")
plt.ylabel("y")

plt.suptitle("Bresenham's line algorithm")
plt.tight_layout()
plt.subplots_adjust(top=0.9)
plt.show()
